package bibleReader.tests;

import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.SimpleBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/*
 * Tests for the Verse class.
 * @author ?
 * @modified Lucy Katter and Grace Glagola
 */


public class Stage01StudentVerseTest {
	private Verse John1_5_v1;
	private Verse John1_5_v2;
	private Verse Luke2_3;
	private Verse Luke2_4;
	private Verse Mark4_6;
	private Verse Mark5_6;
	private Verse Job3_2;

	private Reference Lk2_3;
	private Reference Mk4_6;
	private Reference Jn1_5;

	/*
	* Creates varied Verse objects to test and references to add to the verse
	* objects that are created with the constructor with the parameter that has reference in it.
	*/

	@Before
	public void setUp() throws Exception {
		Jn1_5 = new Reference(BookOfBible.John, 1, 5);
		John1_5_v1 = new Verse(Jn1_5, "John text");
		John1_5_v2 = new Verse(BookOfBible.John, 1, 5, "John text");
		Lk2_3 = new Reference(BookOfBible.Luke, 2, 3);
		Luke2_3 = new Verse(Lk2_3, "Luke Ch2V3 text");
		Luke2_4 = new Verse(BookOfBible.Luke, 2, 4, "Luke Ch2V4 text");
		Mk4_6 = new Reference(BookOfBible.Mark, 4, 6);
		Mark4_6 = new Verse(Mk4_6, "Mark Ch4 text");
		Mark5_6 = new Verse(BookOfBible.Mark, 5, 6, "Mark Ch5 text");
		Job3_2 = new Verse(BookOfBible.Job, 3, 2, "Job text");

	}

	
	// Tests the method GetReference() from the Verse class with various verses.

	@Test
	public void testGetReference() {
		assertEquals(Lk2_3, Luke2_3.getReference());
		assertEquals(Mk4_6, Mark4_6.getReference());
		assertFalse(Mark5_6.getReference().equals(Mark4_6.getReference()));
		assertFalse(Luke2_3.getReference().equals(Luke2_4.getReference()));
	}
	
	// Tests the method GetText() from the Verse class with various verses.

	@Test
	public void testGetText() {
		assertEquals("John text", John1_5_v2.getText());
		assertFalse("Mark text".equals(Luke2_3.getText()));
		assertEquals("Job text", Job3_2.getText());
		assertFalse("Luke Ch2V3 text".equals(Luke2_4.getText()));
	}

	// Tests the method toString() from the Verse class with various verses.
	@Test
	public void testToString() {
		assertEquals("John 1:5 John text", John1_5_v2.toString());
		assertEquals("Mark 5:6 Mark Ch5 text", Mark5_6.toString());
		assertFalse("This is wrong".equals(Job3_2.toString()));
		assertFalse("Mark Ch4 text".equals(Mark5_6.toString()));
	}

	/* Tests the equals() method in the Verse class to make sure that some verses are 
	*equal to one another, and others are not equal. Additionally, the hashCode() from the 
	*Verse class is used here also.
	*/
	@Test
	public void testEquals() {
		Verse Exodus5_4v1 = new Verse(BookOfBible.Exodus, 5, 4, "Exodus text");
		Verse Exodus5_4v2 = new Verse(BookOfBible.Exodus, 5, 4, "Exodus text");
		assertTrue(Exodus5_4v1.equals(Exodus5_4v2));
		assertTrue(Exodus5_4v1.hashCode() == Exodus5_4v2.hashCode());
		assertNotSame(BookOfBible.Luke, Lk2_3);
		assertFalse(BookOfBible.Luke.hashCode() == Lk2_3.hashCode());
	}

	/* Tests the compareTo() method in the Verse class to mathematically show the comparison 
	*between the different verses in the Bible. 
	*/
	@Test
	public void testCompareTo() {
		Verse Psalms5_4v1 = new Verse(BookOfBible.Psalms, 5, 4, "Psalms text");
		Verse Psalms5_4v2 = new Verse(BookOfBible.Psalms, 5, 4, "Psalms text");
		assertTrue(Luke2_3.compareTo(Mark4_6) > 0);
		assertFalse(new Verse(BookOfBible.Job, 2, 3, "Job text").compareTo(Job3_2) > 0);
		assertTrue(Luke2_3.compareTo(Luke2_4) < 0);
		assertTrue(Psalms5_4v1.compareTo(Psalms5_4v2) == 0);
	}
	
	/*Tests the sameReference() method in the Verse class to see if the references contain the 
	*same information
	*/
	
	@Test
	public void testSameReference() {
		Reference Mk5_6v2 = new Reference(BookOfBible.Mark, 5, 6);
		Verse Markv2 = new Verse(Mk5_6v2, "Mark v2 text");
		assertEquals(Mark5_6.getReference(), Markv2.getReference());
		assertFalse(Mark5_6.getReference().equals(Mark4_6.getReference()));
		Reference Jd2_7 = new Reference(BookOfBible.Jude, 2, 7);
		Verse Jude2_7 = new Verse(Jd2_7, "Jude text");
		assertFalse(Luke2_3.getReference().equals(Jude2_7));
	}

	@After
	public void tearDown() throws Exception {

	}

}
