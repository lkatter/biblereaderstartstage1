package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.SimpleBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author ?
 * @modified Grace Glagola and Lucy Katter
 */

public class Stage01StudentReferenceTest {

	// A few sample references to get you started.
	private Reference ruth1_2;
	private Reference gen3_4;
	private Reference rev5_6;
	private Reference jdg9_4;

	/*
	 * Creates varied Reference objects to test and references to add to the
	 * reference objects that are created with the constructor with the
	 * parameter that has reference in it.
	 */
	
	@Before
	public void setUp() throws Exception {
		ruth1_2 = new Reference(BookOfBible.Ruth, 1, 2);
		gen3_4 = new Reference(BookOfBible.Genesis, 3, 4);
		rev5_6 = new Reference(BookOfBible.Revelation, 5, 6);
		jdg9_4 = new Reference(BookOfBible.Judges, 9, 4);
	}



	// Tests the method getBook() from the Reference class with various books
	// and references.

	@Test

	public void testGetBook() {
		assertEquals("Ruth", ruth1_2.getBook());
		assertEquals("Genesis", gen3_4.getBook());
		assertEquals("Revelation", rev5_6.getBook());
		assertEquals("Judges", jdg9_4.getBook());

	}

	/*
	 * Tests the getBookOfBible() method from the Reference class with various
	 * books and references.
	 */

	@Test
	public void testGetBookOfBible() {
		assertEquals(BookOfBible.Ruth, ruth1_2.getBookOfBible());
		assertEquals(BookOfBible.Genesis, gen3_4.getBookOfBible());
		assertEquals(BookOfBible.Revelation, rev5_6.getBookOfBible());
		assertEquals(BookOfBible.Judges, jdg9_4.getBookOfBible());

	}

	/*
	 * Tests the method getChapter() from the Reference class with different
	 * books, references, and chapters
	 */
	
	@Test
	public void testGetChapter() {
		assertEquals(1, ruth1_2.getChapter());
		assertEquals(3, gen3_4.getChapter());
		assertEquals(5, rev5_6.getChapter());
		assertEquals(9, jdg9_4.getChapter());

	}
	
	/*
	 * Tests the method getVerse() from the Reference class with different
	 * books, references, and verses
	 */

	@Test
	public void testGetVerse() {
		assertEquals(2, ruth1_2.getVerse());
		assertEquals(4, gen3_4.getVerse());
		assertEquals(6, rev5_6.getVerse());
		assertEquals(4, jdg9_4.getVerse());

	}
	
	/*
	 * Tests the method toString() from the Reference class to make sure that
	 * the method correctly shows the information of various books, chapters,
	 * verses as a String
	 */

	@Test
	public void testToString() {
		assertEquals("Ruth 1:2", ruth1_2.toString());
		assertEquals("Genesis 3:4", gen3_4.toString());
		assertEquals("Revelation 5:6", rev5_6.toString());
		assertEquals("Judges 9:4", jdg9_4.toString());
	}
	
	/*
	 * Tests the equals() method in the Reference class to make sure that some
	 * references are equal to one another, and others are not equal.
	 * Additionally, the hashCode() from the Reference class is used here also.
	 */

	@Test
	public void testEquals() {
		Reference Job1_8v1 = new Reference(BookOfBible.Job, 1, 8);
		Reference Job1_8v2 = new Reference(BookOfBible.Job, 1, 8);
		assertTrue(Job1_8v1.equals(Job1_8v2));
		assertTrue(Job1_8v2.hashCode() == Job1_8v1.hashCode());
		assertNotSame(BookOfBible.Ruth, ruth1_2);
		assertFalse(BookOfBible.Ruth.hashCode() == ruth1_2.hashCode());

	}

	/*
	 * Tests the compareTo() method in the Reference class to mathematically
	 * show the comparison * between the different references to
	 * books/chapters/verses in the Bible.
	 */
	
	@Test
	public void testcompareTo() {
		Reference ruth1_2v2 = new Reference(BookOfBible.Ruth, 1, 2);
		assertTrue(ruth1_2.compareTo(gen3_4) > 0);
		assertFalse(new Reference(BookOfBible.Genesis, 4, 4).compareTo(gen3_4) < 0);
		assertTrue(new Reference(BookOfBible.Genesis, 3, 5).compareTo(gen3_4) > 0);
		assertTrue(ruth1_2.compareTo(ruth1_2v2) == 0);

	}

	@After
	public void tearDown() throws Exception {

	}

}

