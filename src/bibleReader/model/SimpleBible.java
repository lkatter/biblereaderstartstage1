package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Lucy Katter (provided the implementation)
 */

public class SimpleBible {
	ArrayList<Verse> versesL;
	String version;
	String title;

	/**
	 * Create a new Bible with the given verses and sets the version and title.
	 * 
	 * @param verses A VerseList of the verses of this version of the Bible whose
	 *               version is set to the version of the Bible they came from and
	 *               whose description is set to the title of the Bible.
	 */
	
	public SimpleBible(VerseList verses) {

		version = verses.getVersion();  //get version and title from VerseList class
		title = verses.getDescription();
		new VerseList(version, title, versesL = new ArrayList<>());
		for (Verse verse : verses) {
			versesL.add(verse);
		}

	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	
	public int getNumberOfVerses() {

		return versesL.size();
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	
	public String getVersion() {

		return version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */

	public String getTitle() {

		return title;
	}

	/**
	 * @param ref the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */

	public boolean isValid(Reference ref) {
		for (Verse verse : versesL) {
			if (verse.getReference().equals(ref)) {
				return true; 
			}
		}

		return false; //the reference was not found in any of the verses in the list
	}

	/**
	 * Return the text of the verse with the given reference
	 * 
	 * @param ref the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */

	public String getVerseText(Reference ref) {
		for (Verse verse : versesL) {
			if (verse.getReference().equals(ref)) {
				return verse.getText();
			}
		}
		return null;
	}

	/**
	 * Return a Verse object for the corresponding Reference.
	 * 
	 * @param ref A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	public Verse getVerse(Reference ref) {
		for (Verse verse : versesL) {
			if (verse.getReference().equals(ref)) {
				return verse;
			}
		}
		return null;

	}

	/**
	 * @param book    the book of the Bible
	 * @param chapter the chapter of the book
	 * @param verse   the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if the
	 *         reference is invalid.
	 */

	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		for (Verse theVerse : versesL) {
			if (theVerse.getReference().equals(new Reference(book, chapter, verse))) {
				return theVerse;
			}
		}
		return null;

	}
}
