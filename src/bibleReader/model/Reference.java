package bibleReader.model;


/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Lucy Katter (provided the implementation)
 */
public class Reference implements Comparable<Reference> {

	private BookOfBible book;
	private int chapter;
	private int verse;

	public Reference(BookOfBible book, int chapter, int verse) {
		
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	/**
	 * Returns the title of the BookOfBible object for this Reference as a string.
	 * 
	 * @return A reference to the title of the BookOfBible for this Reference.
	 */

	public String getBook() {

		return book.toString();

	}

	/**
	 * Returns the BookOfBible object for this Reference.
	 * 
	 * @return A reference to the BookOfBible for this Reference.
	 */

	public BookOfBible getBookOfBible() {

		return book;
	}

	/**
	 * Returns the Chapter object for this Reference.
	 * 
	 * @return A reference to the Chapter for this Reference.
	 */

	public int getChapter() {

		return chapter;
	}

	/**
	 * Returns the Verse object for this Reference.
	 * 
	 * @return A reference to the Verse for this Reference.
	 */

	public int getVerse() {

		return verse;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse" For
	 * instance, "Genesis 2:3".
	 */

	@Override
	public String toString() {

		return book.toString() + " " + chapter + ":" + verse;
	}

	/**
	 * Should return true if and only if they have the same book title, chapter, and
	 * verse.
	 */

	@Override
	public boolean equals(Object other) {
		
		if (other instanceof Reference) { //makes sure the object is Reference type
			Reference otherRef = (Reference) other;
			if (otherRef.getBook().equals(this.getBook()) && otherRef.getChapter() == (this.getChapter())
					&& otherRef.getVerse() == this.getVerse()) {
				return true;
			}
		}
		return false; //if book, chapter, and verse are not all the same, this is a different reference
	}

	/**
	 * Should return the hashcode for this verse.
	 */

	@Override
	public int hashCode() {

		return this.toString().hashCode(); 

	}

	@Override

	/**
	 * Returns an integer that resulted from the comparison method. So long as the integer is either negative
	 * for References that are before the other reference or positive for references that are after the other 
	 * reference, it can be any integer (and so long as 0 is the integer that represents when two Reference
	 * objects are the same).
	 * 
	 * @param otherRef the other Reference.
	 * @return an integer that resulted from the comparison methods.
	 */

	public int compareTo(Reference otherRef) {
		
		if (!this.getBookOfBible().equals(otherRef.getBookOfBible())) {
			
			if (this.getBookOfBible().ordinal() > otherRef.getBookOfBible().ordinal()) {
				
				return 17; //if this book comes after the other book in the enum list, return a positive number (in this case, 17)
				
			} else if (this.getBookOfBible().ordinal() < otherRef.getBookOfBible().ordinal()) {
				
				return -17; //if this book comes before the other book, return a negative number (in this case, -17)
			}
		} else if (this.getChapter() != otherRef.getChapter()) {
			
			if (this.getChapter() > otherRef.getChapter()) {
				
				return 17;
				
			} else if (this.getChapter() < otherRef.getChapter()) {
				
				return -17;
			}
		} else if (this.getVerse() != otherRef.getVerse()) {
			
			if (this.getVerse() > otherRef.getVerse()) {
				
				return 17;
				
			} else if (this.getVerse() < otherRef.getVerse()) {
				
				return -17;
			}
		}

		return 0; //if this reference has the same book, chapter, and verse, return 0
	}
}
