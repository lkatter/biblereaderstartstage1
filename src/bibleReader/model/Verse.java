package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Lucy Katter (provided the implementation)
 */

public class Verse implements Comparable<Verse> {

	private Reference ref;
	private String text;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param ref The reference for the verse
	 * @param text The text of the verse
	 */
	
	public Verse(Reference ref, String text) {
		
		this.ref = ref;
		this.text = text;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 * @param text    The text of the verse
	 */
	
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		
		ref = new Reference(book, chapter, verse); //creates a new reference out of the parameters of this constructor
		this.text = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	
	public Reference getReference() {

		return ref;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	
	public String getText() {

		return text;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 */
	
	@Override
	public String toString() {
		//make the representation have a space after book before chapter and verse, and after verse before text
		return ref.getBook().toString() + " "+ ref.getChapter() + ":" + ref.getVerse() +" "+ text;

	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	
	@Override
	public boolean equals(Object other) {
		
		if (other instanceof Verse) {
			Verse otherVerse = (Verse) other;
			if (otherVerse.getReference().equals(this.getReference())
					&& otherVerse.getText().contentEquals(this.getText())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		
		return this.toString().hashCode();
	}

	/**
	 * Returns an integer that resulted from the comparison method. So long as the integer is either negative for Verses
	 * that are before the other verse or positive for verses that are after the other verse, 
	 * it can be any integer (and so long as 0 is the integer that represents when two Verse
	 * objects are the same).
	 * 
	 * @param other the other Verse.
	 * @return an integer that resulted from the comparison methods.
	 */
	
	@Override
	public int compareTo(Verse other) {
		
		if(ref.compareTo(other.ref)!=0) {
	
			return ref.compareTo(other.ref);
		}
		else if (!this.getText().equals(other.getText())){
			
			if (this.getText().compareTo(other.getText())>0) {
				return 17;
			}
			else {
				return -17;
			}
		}
		return 0;
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. 
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		
		return getReference().equals(other.getReference());
	}

}
